<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use Validator;

class UserController extends Controller
{
    public function myuser(){
          return response()->json(User::get(), 200);
    }

    public function myuserByID($id){
        $myUser = User::find($id);
        if (is_null($myUser)){
            return response()->json(["message" => "Record Not Found"], 404);   
        }   
        return response()->json(User::find($id), 200);
    }

    public function createUser(Request $request){
        $myuser = User::create($request->all());
        return response()->json($myuser, 201);
    }

    public function updateUser(Request $request, $id){
        $rules = [
            "username" => 'required|min:5'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()){
            return response()->json($validator->errors(), 400);
        }


        $myUser = User::find($id);
        if (is_null($myUser)){
            return response()->json(["message" => "Record Not Found"], 404);   
        }
        $myUser->update($request->all());
        return response()->json($myUser, 200);
    }

    public function deleteUser(Request $request, $id){
        $myUser = User::find($id);
        if (is_null($myUser)){
            return response()->json(["message" => "Record Not Found"], 404);   
        }
       $myUser->delete();
        return response()->json(null, 204);
    }
}
