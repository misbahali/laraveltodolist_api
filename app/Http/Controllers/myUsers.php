<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Models\User;
use Validator;

class myUsers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(User::get(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            "username" => 'required|min:5',
            "email" => 'required',
            "password" => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $myuser = User::create($request->all());
        return response()->json($myuser, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $myUser = User::find($id);
        if (is_null($myUser)){
            return response()->json(["message" => "Record Not Found"], 404);   
        }
        return response()->json(User::find($id), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            "username" => 'required|min:5'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()){
            return response()->json($validator->errors(), 400);
        }


        $myUser = User::find($id);
        if (is_null($myUser)){
            return response()->json(["message" => "Record Not Found"], 404);   
        }
        $myUser->update($request->all());
        return response()->json($myUser, 200);
   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $myUser = User::find($id);
        if (is_null($myUser)){
            return response()->json(["message" => "Record Not Found"], 404);   
        }
       $myUser->delete();
        return response()->json(null, 204);
    
    }
}
