<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Todo extends Authenticatable 
    
{
    protected $table = "todo";
    public $timestamps = false;


    protected $fillable = [
         'user_id',
         'title',
         'description',
         'date_created',
         'completed'
    ];

}
