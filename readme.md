# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
	Backend API created with Express, Node and Laravel - Containes basic CRUD functionality for ToDo Application.
	This includes Basic Authentication
* Version
	1.0.0
	
	
### How do I get set up? ###

* Summary of set up
	Make sure you have PHP 7+ and Laravel 5+
	Download or clone the repo
	make sure you have the databases configured (as explained below)
	Go to the root folder and run "php artisan serve"
	
* Configuration

* Dependencies
* Database configuration
	-> Schema named "myproj"
		-> tables required: users (for basic auth)
							todo (containes the notes)
							
	These tables can be changed as per your requirement in the .env files and the controller files
	
	
	Users Table:
	id (PK, AI) INT
	email VARCHAR64
	username VARCHAR64
	password VARCHAR64
	
	
	Todo Table:
	id (PK, AI) INT
	user_id INT
	title VARCHAR64
	description TEXT
	date_created DATE
	completed TINYINT(1) [Boolean, 0 for false, non zero for true]

### How do I use this? ###

* Overview
	You can consume the api through api front-end framework
	If you want a quick front-end consumer of this api go to https://bitbucket.org/misbahali/reactfrontend/
	
	
### Who do I talk to? ###

* Repo owner or admin
	Muhammad Misbah Ali - muhammadmisbahali@gmail.com
